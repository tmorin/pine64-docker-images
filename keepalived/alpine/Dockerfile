ARG arch="aarch64"
ARG distribution="alpine"
ARG distribution_version="3.8"
FROM balenalib/${arch}-${distribution}:${distribution_version}

# Use osixia/light-baseimage
# sources: https://github.com/osixia/docker-light-baseimage
#FROM osixia/alpine-light-baseimage:0.1.5
#MAINTAINER Bertrand Gouny <bertrand.gouny@osixia.net>

# Keepalived version
ARG version=1.4.5

COPY . /container

RUN [ "cross-build-start" ]

RUN /container/build.sh

RUN [ "cross-build-end" ]

ENV LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

RUN [ "cross-build-start" ]

# Download, build and install Keepalived
RUN apk --no-cache add \
       autoconf \
       curl \
       gcc \
       ipset \
       ipset-dev \
       iptables \
       iptables-dev \
       libnfnetlink \
       libnfnetlink-dev \
       libnl3 \
       libnl3-dev \
       make \
       musl-dev \
       openssl \
       openssl-dev \
    && curl -o keepalived.tar.gz -SL http://keepalived.org/software/keepalived-${version}.tar.gz \
    && mkdir -p /container/keepalived-sources \
    && tar -xzf keepalived.tar.gz --strip 1 -C /container/keepalived-sources \
    && cd container/keepalived-sources \
    && ./configure --disable-dynamic-linking \
    && make && make install \
    && cd - && mkdir -p /etc/keepalived \
    && rm -f keepalived.tar.gz \
    && rm -rf /container/keepalived-sources \
    && apk --no-cache del \
        autoconf \
        curl \
        gcc \
        ipset-dev \
        iptables-dev \
        libnfnetlink-dev \
        libnl3-dev \
        make \
        musl-dev \
        openssl-dev

RUN /container/tool/install-service

RUN [ "cross-build-end" ]

ENTRYPOINT ["/container/tool/run"]