#!/bin/bash

ARCHITECTURES="armv7hf aarch64"

REPOSITORY=registry.gitlab.com/tmorin/pine64-docker-images

NAME=$1
DIST=$2
IMAGE=${REPOSITORY}/${NAME}-${DIST}
VERSION_FULL=$3
VERSION_MAJOR=$(echo ${VERSION_FULL} | sed -E "s/([0-9]*)\.[0-9]*\.[0-9]*/\1/")
VERSION_MINOR=$(echo ${VERSION_FULL} | sed -E "s/([0-9]*\.[0-9]*)\.[0-9]*/\1/")

shift
shift
shift

for arch in ${ARCHITECTURES}; do
    IMAGE_FULL=${IMAGE}:${arch}-${VERSION_FULL}
    IMAGE_MAJOR=${IMAGE}:${arch}-${VERSION_MAJOR}
    IMAGE_MINOR=${IMAGE}:${arch}-${VERSION_MINOR}
    IMAGE_LATEST=${IMAGE}:${arch}

    echo "--------- CHECK $IMAGE_FULL"

    docker --config .docker pull ${IMAGE_FULL}

    if [ $? -ne 0 ]; then
        DIRECTORY=${NAME}/${DIST}
        echo "--------- BUILD $IMAGE_FULL from $DIRECTORY"

        docker --config .docker build \
            -f ${DIRECTORY}/Dockerfile \
            --tag ${IMAGE_FULL} \
            --build-arg version=${VERSION_FULL} \
            --build-arg arch=${arch} \
            $@ ${DIRECTORY} \
        && docker --config .docker tag ${IMAGE_FULL} ${IMAGE_MAJOR} \
        && docker --config .docker tag ${IMAGE_FULL} ${IMAGE_MINOR} \
        && docker --config .docker tag ${IMAGE_FULL} ${IMAGE_LATEST} \
        && docker --config .docker push ${IMAGE_FULL} \
        && docker --config .docker push ${IMAGE_MAJOR} \
        && docker --config .docker push ${IMAGE_MINOR} \
        && docker --config .docker push ${IMAGE_LATEST}

        if [ $? -ne 0 ]; then
            exit 1
        fi
    fi

done

function push_manifest() {
    local manifest_list=${IMAGE}:$1

    local manifest=""
    for arch in ${ARCHITECTURES}; do
        manifest="$manifest ${IMAGE}:${arch}"
        if [ $1 != 'latest' ]; then
            manifest="$manifest-$1"
        fi
    done

    docker --config .docker manifest inspect ${manifest_list}
    if [ $? -eq 0 ]; then
        exit 0
    fi

    docker --config .docker manifest create --amend ${manifest_list} ${manifest}
    if [ $? -ne 0 ]; then
        exit 2
    fi

    for arch in ${ARCHITECTURES}; do
        case ${arch} in
        armv7hf)
            docker_arch=arm
            ;;
        aarch64)
            docker_arch=arm64
            ;;
        esac

        if [ $1 == 'latest' ]; then
            docker --config .docker manifest annotate ${manifest_list} ${IMAGE}:${arch} --arch ${docker_arch}
            if [ $? -ne 0 ]; then
                exit 3
            fi
        else
            docker --config .docker manifest annotate ${manifest_list} ${IMAGE}:${arch}-$1 --arch ${docker_arch}
            if [ $? -ne 0 ]; then
                exit 3
            fi
        fi
    done

    docker --config .docker manifest push -p ${manifest_list}
    if [ $? -ne 0 ]; then
        exit 4
    fi
}

for VERSION in ${VERSION_FULL} ${VERSION_MAJOR} ${VERSION_MINOR} latest; do
    push_manifest ${VERSION}
done

exit 0
