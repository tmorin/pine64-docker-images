#!/usr/bin/env bash

VERSION="${1//\//_}"
VERSION="${VERSION:-latest}"
LATEST="${2}"

if [[ "${VERSION}" =~ ^v?[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    VERSION=$(echo ${VERSION} | sed -E "s/v?([0-9]+\.[0-9]+\.[0-9]+)/\1/")
    VERSION_MAJOR=$(echo ${VERSION} | sed -E "s/([0-9]+)\.[0-9]+\.[0-9]+/\1/")
    VERSION_MINOR=$(echo ${VERSION} | sed -E "s/([0-9]+\.[0-9]+)\.[0-9]+/\1/")
    VERSIONS="${VERSION} ${VERSION_MAJOR} ${VERSION_MINOR}"
    if [[ ! -z ${LATEST} ]]; then
        VERSIONS="${VERSIONS} latest"
    fi
else
    VERSIONS="${VERSION}"
fi

BASE_IMAGE_GRAFANA="registry.gitlab.com/tmorin/pine64-docker-images/grafana-debian"

ARCHITECTURES="amd64 arm64 arm"

BASED_ARCH=$(docker system info -f "{{.Architecture}}")
if [[ ${BASED_ARCH} == "x86_64" ]]; then
    BASED_ARCH="amd64"
elif [[ ${BASED_ARCH} == "aarch64" ]]; then
    BASED_ARCH="arm64"
fi

echo VERISONS: ${VERSIONS}
echo BASE_IMAGE_GRAFANA: ${BASE_IMAGE_GRAFANA}
echo BASED_ARCH: ${BASED_ARCH}

# dockerfile architectures: amd64 armv7hf aarch64
# see https://www.balena.io/docs/reference/base-images/base-images/#-a-name-image-tree-a-balena-image-trees

# docker architecture: amd64(x86_64) arm arm64(aarch64)

function build() {
    local tags=""
    local docker_arch=$1
    local dockerfile_arch=${docker_arch}
    local pkg_arch=${docker_arch}

    case ${docker_arch} in
        arm)
            dockerfile_arch=armv7hf
            pkg_arch=armhf
            ;;
        arm64)
            dockerfile_arch=aarch64
            ;;
    esac

    local dockerfile="grafana/debian/Dockerfile"
    if [[ ${BASED_ARCH} != ${docker_arch} ]]; then
        echo "cross build for ${docker_arch}"
        mkdir -p .tmp
        sed -e 's/#cross//g' ${dockerfile} > .tmp/Dockerfile;
        dockerfile=".tmp/Dockerfile"
    fi

    local tag="${BASE_IMAGE_GRAFANA}:${docker_arch}-${VERSION}"

    docker pull ${tag}
    if [[ $? -eq 0 ]]; then
        return 0
    fi

    tags="${tags} ${tag}"
    echo "build $tag"
    docker build -f ${dockerfile} \
        --build-arg dockerfile_arch=${dockerfile_arch} \
        --build-arg pkg_arch=${pkg_arch} \
        --build-arg pkg_version=${VERSION} \
        --no-cache -t ${tag} grafana/debian
    if [[ $? -ne 0 ]]; then
        exit 1
    fi

    if [[ -n ${VERSION_MAJOR} ]]; then
        local tag_major="${BASE_IMAGE_GRAFANA}:${docker_arch}-${VERSION_MAJOR}"
        tags="${tags} ${tag_major}"
        local tag_minor="${BASE_IMAGE_GRAFANA}:${docker_arch}-${VERSION_MINOR}"
        tags="${tags} ${tag_minor}"
        echo "tag $tag_major"
        echo "tag $tag_minor"
        docker tag ${tag} ${tag_major} \
            && docker tag ${tag} ${tag_minor}
        if [[ $? -ne 0 ]]; then
            exit 2
        fi
        if [[ ! -z ${LATEST} ]]; then
            local tag_latest="${BASE_IMAGE_GRAFANA}:${docker_arch}-latest"
            tags="${tags} ${tag_latest}"
            echo "tag $tag_latest"
            docker tag ${tag} ${tag_latest}
            if [[ $? -ne 0 ]]; then
                exit 2
            fi
        fi
    fi

    for tag in ${tags}; do
        echo "push ${tag}"
        docker push ${tag}
        if [[ $? -ne 0 ]]; then
            exit 3
        fi
    done
}

function manifest() {
    local version=$1
    local main_tag="${BASE_IMAGE_GRAFANA}:$VERSION"

    local tags=""
    for architecture in ${ARCHITECTURES}; do
        local tag="${BASE_IMAGE_GRAFANA}:${architecture}-$version"
        tags="${tags} ${tag}"
    done

    echo "build manifest for ${main_tag} ${tags}"

    docker --config .docker manifest create --amend ${main_tag} ${tags}

    for architecture in ${ARCHITECTURES}; do
        local tag="${BASE_IMAGE_GRAFANA}:${architecture}-$version"
        echo "annotate ${tag} with ${architecture}"
        docker --config .docker manifest annotate ${main_tag} ${tag} --arch ${architecture} --os linux
    done

    docker --config .docker manifest push -p ${main_tag}
    if [[ $? -ne 0 ]]; then
        exit 4
    fi
}

for architecture in ${ARCHITECTURES}; do
    build ${architecture}
    if [[ $? -ne 0 ]]; then
        exit 5
    fi
done

for version in ${VERSIONS}; do
    manifest ${version}
    if [[ $? -ne 0 ]]; then
        exit 6
    fi
done
