#!/bin/bash

exec sudo -u ${SYNCTHING_USER} bash -c "syncthing -home=${SYNCTHING_HOME}/config -no-restart"
